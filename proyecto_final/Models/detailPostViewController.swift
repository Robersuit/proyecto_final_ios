//
//  detailPostViewController.swift
//  proyecto_final
//
//  Created by Desarrollo Web on 25/05/19.
//  Copyright © 2019 roberto_guiza. All rights reserved.
//

import UIKit

class detailPostViewController: UIViewController {

    
    @IBOutlet var txtTitle: UITextView!
    @IBOutlet var txtBody: UITextView!
    
    var titlePost: String?
    var bodyPost: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtTitle.text = titlePost
        txtBody.text = bodyPost
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
