//
//  postModel.swift
//  proyecto_final
//
//  Created by Desarrollo Web on 25/05/19.
//  Copyright © 2019 roberto_guiza. All rights reserved.
//

import Foundation

class PostModel{
    //Atributos
    
    var id: Int = -1
    var title: String = ""
    var body: String = ""
    
    //Constructor
    init(title: String, body: String){
        self.title = title
        self.body = body
    }
    
}
