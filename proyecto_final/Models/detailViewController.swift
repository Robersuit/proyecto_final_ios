//
//  detailViewController.swift
//  proyecto_final
//
//  Created by Desarrollo Web on 25/05/19.
//  Copyright © 2019 roberto_guiza. All rights reserved.
//

import UIKit

class detailViewController: UIViewController {

    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblUser: UILabel!
    @IBOutlet var viewPhoto: UIImageView!
    
    var user: UserModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        lblName.text = user?.name
        lblUser.text = user?.user
        viewPhoto.image = user?.photo
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
