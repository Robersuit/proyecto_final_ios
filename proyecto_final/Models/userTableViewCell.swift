//
//  userTableViewCell.swift
//  proyecto_final
//
//  Created by Desarrollo Web on 25/05/19.
//  Copyright © 2019 roberto_guiza. All rights reserved.
//

import UIKit

class userTableViewCell: UITableViewCell {

    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblUser: UILabel!
    @IBOutlet var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
