//
//  userModel.swift
//  proyecto_final
//
//  Created by Desarrollo Web on 24/05/19.
//  Copyright © 2019 roberto_guiza. All rights reserved.
//

import Foundation
import UIKit

class UserModel{
    
    var name:       String
    var user:       String
    var password:   String
    var photo =       UIImage()
    
    init(name: String, user: String, password: String, photo: UIImage){
        
        self.name = name
        self.user = user
        self.password = password
        self.photo = photo
    }
}
