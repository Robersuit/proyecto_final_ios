//
//  postsViewController.swift
//  proyecto_final
//
//  Created by Desarrollo Web on 25/05/19.
//  Copyright © 2019 roberto_guiza. All rights reserved.
//

import UIKit

class postsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var posts = [PostModel]()
    var selectedIndex = -1
    
    
    @IBOutlet var tblViewPosts: UITableView!
    @IBOutlet var loader: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblViewPosts.dataSource = self
        tblViewPosts.delegate = self
        
        getPosts()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellPost")
        cell?.textLabel!.text = posts[indexPath.row].title
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "detailPost", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        let destino = segue.destination as! detailPostViewController
        destino.titlePost = posts[selectedIndex].title
        destino.bodyPost = posts[selectedIndex].body
    }
    
    func getPosts(){
        loader.startAnimating()
        
        //Dirección
        let urlString = "https://jsonplaceholder.typicode.com/posts"
        //General URL
        let url = URL(string: urlString)!
        
        let session = URLSession.shared
        //Objeto request
        let request = URLRequest(url: url)
        
        let task = session.dataTask(with: request) { (data, response, error) in
            if (error != nil){
                return
            }
            
            if (data == nil){
                return
            }
            
            //Si no hay error y si existe información...
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [[String: Any]]
                print(json)
                
                //Descomponer json
                for post in json {
                    let title       = post["title"] as! String
                    let body        = post["body"] as! String

                    //Lenar arreglo de usuarios
                    self.posts.append(PostModel(title: title, body: body))
                }
                
                DispatchQueue.main.async {
                    self.tblViewPosts.reloadData()
                    self.loader.stopAnimating()
                }
            } catch let error {
                print("Error al deserializar: \(error.localizedDescription)")
            }
            
        }
        task.resume()
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
