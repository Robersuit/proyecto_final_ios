//
//  newUserViewController.swift
//  proyecto_final
//
//  Created by Desarrollo Web on 24/05/19.
//  Copyright © 2019 roberto_guiza. All rights reserved.
//

import UIKit

class newUserViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    
    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtUser: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var imageView: UIImageView!
    
    var imagePickerController : UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func hideKeyboard(){
        view.endEditing(true)
    }
    
    @IBAction func actPhoto(_ sender: Any) {
        imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePickerController.dismiss(animated: true, completion: nil)
        let image = info[.originalImage] as! UIImage
        imageView.image = image
    }
    
    @IBAction func actRegister(_ sender: Any) {
        
        if((txtName.text?.isEmpty)!){
            txtName.backgroundColor = UIColor.red
        }else{
            if((txtUser.text?.isEmpty)!){
                txtUser.backgroundColor = UIColor.red
            }else{
                if((txtPassword.text?.isEmpty)!){
                    txtPassword.backgroundColor = UIColor.red
                }else{
                    if(imageView.image == nil){
                        //Generar alerta
                        let alerta = UIAlertController(title: "Error", message: "Debes de seleccionar una imagen", preferredStyle: .actionSheet)
                        //Agregar acciones
                        alerta.addAction(UIAlertAction(title: "Reintentar", style: .destructive, handler: nil))
                        //Mostrar alerta
                        self.present(alerta, animated: true, completion: nil)
                    }else{
                        txtName.backgroundColor = UIColor.white
                        txtUser.backgroundColor = UIColor.white
                        txtPassword.backgroundColor = UIColor.white
                        
                        //Mostrar confirmación
                        let alert =  UIAlertController(title: "Confirmación", message: "Deseas agregar al usuario: \(txtUser.text!)", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler:{
                            (action) in
                            //Agregar usuario
                            
                            UserHelper.shared.users.append(UserModel(name: self.txtName.text!, user: self.txtUser.text!, password: self.txtPassword.text!, photo: self.imageView.image!))
                            
                            //(Limpiar campos)
                            self.txtName.text = ""
                            self.txtUser.text = ""
                            self.txtPassword.text = ""
                            self.imageView.image = nil
                            
                            print(UserHelper.shared.users.description)
                            print(UserHelper.shared.users.count)
                            
                        }))
                        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
            }
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
