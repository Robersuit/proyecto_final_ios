//
//  ViewController.swift
//  proyecto_final
//
//  Created by Desarrollo Web on 24/05/19.
//  Copyright © 2019 roberto_guiza. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet var txtUser: UITextField!
    @IBOutlet var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Inicializar el evento de touch para ocultar teclado
        let tap = UITapGestureRecognizer(target: self, action: #selector(ocultarTeclado))
        view.addGestureRecognizer(tap)
        
    }
    
    //Metodo para ocultar el teclado
    @objc func ocultarTeclado(){
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    @IBAction func Login(_ sender: UIButton) {
        var loginFlag = false
        for usuarios in UserHelper.shared.users{
            if  (usuarios.user == txtUser.text && usuarios.password == txtPassword.text){
                loginFlag = true
            }
        }
        if  (loginFlag){
            print("Generar alerta de bienvenida")
            
            //Generar alerta
            let alerta = UIAlertController(title: "Bienvenido", message: "Ingreso exitoso", preferredStyle: .alert)
            //Agregar acciones
            alerta.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            //Mostrar alerta
            //self.present(alerta, animated: true, completion: nil)
            
            //Asignamos el nombre al objeto UserDefault
            UserDefaults.standard.set(txtUser.text, forKey: "Usuario")
            
            //Cambios de vista
            performSegue(withIdentifier: "loginSegue", sender: nil)
            
        } else{
            print("Generar alerta de error")
            
            //Generar alerta
            let alerta = UIAlertController(title: "Error", message: "El usuario o contraseña ingresado es incorrecto", preferredStyle: .actionSheet)
            //Agregar acciones
            alerta.addAction(UIAlertAction(title: "Reintentar", style: .destructive, handler: nil))
            //Mostrar alerta
            self.present(alerta, animated: true, completion: nil)
        }
    }
    
}

