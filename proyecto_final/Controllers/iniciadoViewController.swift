//
//  iniciadoViewController.swift
//  proyecto_final
//
//  Created by Desarrollo Web on 25/05/19.
//  Copyright © 2019 roberto_guiza. All rights reserved.
//

import UIKit

class iniciadoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet var lblBienvenida: UILabel!
    @IBOutlet var tblUsers: UITableView!
    
    let usuario = UserDefaults.standard.string(forKey: "Usuario") ?? ""
    
    var usersList = UserHelper.shared.users
    var selectedUser: UserModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblBienvenida.text = "Bienvenido "+usuario
        
        tblUsers.dataSource = self
        tblUsers.delegate = self
        
        //TODO getUsers()
        tblUsers.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellUser") as! userTableViewCell
        cell.imgUser.image = usersList[indexPath.row].photo
        cell.lblUser.text = usersList[indexPath.row].user
        cell.lblName.text = usersList[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedUser = usersList[indexPath.row]
        performSegue(withIdentifier: "userDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "userDetail" {
            let vc = segue.destination as! detailViewController
            vc.user = selectedUser
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
